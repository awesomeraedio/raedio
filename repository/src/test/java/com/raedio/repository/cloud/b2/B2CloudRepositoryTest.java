package com.raedio.repository.cloud.b2;

import com.raedio.repository.cloud.AbstractCloudRepository;
import com.raedio.repository.cloud.CloudRepositoryException;
import com.raedio.repository.model.Song;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import synapticloop.b2.B2ApiClient;
import synapticloop.b2.exception.B2ApiException;
import synapticloop.b2.response.B2AuthorizeAccountResponse;
import synapticloop.b2.response.B2BucketResponse;
import synapticloop.b2.response.B2DeleteFileVersionResponse;
import synapticloop.b2.response.B2FileInfoResponse;
import synapticloop.b2.response.B2FileResponse;

/**
 * Unit test for {@code B2CloudRepository}.
 *
 * @author Leonardo A. Mendoza III <leonardo.mendoza@gmail.com>
 */
public class B2CloudRepositoryTest {
    private static final Logger LOG = LoggerFactory.getLogger(B2CloudRepositoryTest.class);
    private static File audioFile;
    private static File musicDirectory;
    private static String bucketId;
    private static String fileId;
    private static B2CloudRepository repository;
    private static B2ApiClient b2ApiClient;
    
    private static B2CloudRepository createMockB2CloudRepository() throws Exception {
        StatusLine statusLine = Mockito.mock(StatusLine.class);
        Mockito.when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);
        String responseString = "{\n" +
"  \"authorizationToken\": \"3_20160803004041_53982a92f631a8c7303e3266_d940c7f5ee17cd1de3758aaacf1024188bc0cd0b_000_20160804004041_0006_dnld\",\n" +
"  \"bucketId\": \"a71f544e781e6891531b001a\",\n" +
"  \"fileNamePrefix\": \"public\"\n" +
"}";
        
        HttpEntity httpEntity = Mockito.mock(HttpEntity.class);
        Mockito.when(httpEntity.getContent()).thenReturn(new ByteArrayInputStream(responseString.getBytes()));
        
        CloseableHttpResponse httpResponse = Mockito.mock(CloseableHttpResponse.class);
        Mockito.when(httpResponse.getStatusLine()).thenReturn(statusLine);
        Mockito.when(httpResponse.getEntity()).thenReturn(httpEntity);
        
        CloseableHttpClient client = Mockito.mock(CloseableHttpClient.class);
        Mockito.when(client.execute(Matchers.any())).thenReturn(httpResponse);
        
        B2ApiClient mockB2ApiClient = Mockito.mock(B2ApiClient.class);
        B2AuthorizeAccountResponse b2AuthorizeAccountResponse = Mockito.mock(B2AuthorizeAccountResponse.class);
        Mockito.when(b2AuthorizeAccountResponse.getDownloadUrl()).thenReturn("https://f700.backblazeb2.com");
        Mockito.when(mockB2ApiClient.listBuckets()).thenReturn(Collections.EMPTY_LIST);
        
        B2FileResponse b2FileResponse = Mockito.mock(B2FileResponse.class);
        Mockito.when(b2FileResponse.getFileName()).thenReturn("muffins.mp3");
        Mockito.when(b2FileResponse.getBucketId()).thenReturn("bucket001");
        
        Mockito.when(mockB2ApiClient.getFileInfo(Matchers.eq(fileId))).thenReturn(b2FileResponse);
        
        B2CloudRepository result = Mockito.spy(new B2CloudRepository(client, mockB2ApiClient, b2AuthorizeAccountResponse));
        Mockito.when(result.getPersistenceUnitName()).thenReturn(AbstractCloudRepository.DEFAULT_PERSISTENCE_UNIT_NAME + "-h2");
        Mockito.doReturn(bucketId).when(result).getBucketName(Matchers.anyString());

        return result;
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
        //System.setProperty("javax.net.debug", "all");
        Properties properties = new Properties();
        properties.load(B2CloudRepositoryTest.class.getClassLoader().getResourceAsStream("B2CloudRepositoryTest.properties"));
        audioFile = new File(properties.getProperty("AUDIO_FILE"));
        assertTrue(audioFile.exists());
        musicDirectory = new File(properties.getProperty("MUSIC_DIRECTORY"));
        assertTrue(musicDirectory.exists());
        bucketId = properties.getProperty("BUCKET_ID");
        fileId = "4_z32e80859d412c16e5a9b0516_f108bfc314527bbf2_d20170122_m153838_c001_v0001037_t0054";
        
        //repository = createMockB2CloudRepository();
        
        b2ApiClient = new B2ApiClient("2889421eab56", "00187ed360a4342975c9a611d215a656ee9ef967de");
        LOG.info("API URL: {}", b2ApiClient.getApiUrl());
        LOG.info("download URL: {}", b2ApiClient.getDownloadUrl());

        repository = repository = Mockito.spy(new B2CloudRepository(bucketId));
        Mockito.when(repository.getPersistenceUnitName()).thenReturn(AbstractCloudRepository.DEFAULT_PERSISTENCE_UNIT_NAME + "-h2");
    }

    @AfterClass
    public static void tearDownClass() throws B2ApiException, IOException {
        //removeTestUploads();
    }

    private static void removeTestUploads() throws B2ApiException, IOException {
        for (B2BucketResponse bucket : b2ApiClient.listBuckets()) {
            LOG.info("Bucket: {}", bucket);
            for(B2FileInfoResponse file : b2ApiClient.listFileNames(bucket.getBucketId()).getFiles()) {
                if (file.getFileName().contains("(Sample)")) {
                    B2DeleteFileVersionResponse response =b2ApiClient.deleteFileVersion(file.getFileName(), file.getFileId());
                    LOG.info("-> Deleted: {}", response);
                }
            }
        }
    }

    @Before
    public void setUp() throws Exception {
        removeTestUploads();
        String query = new StringBuilder("DELETE FROM Song s").toString();
        repository.getEntityManager().getTransaction().begin();
        List<Song> songs = repository.getSongs();
        for (Song song : songs) {
            repository.getEntityManager().remove(song);
        }
        //repository.getEntityManager().createNativeQuery(query).executeUpdate();
        repository.getEntityManager().getTransaction().commit();
    }

    @After
    public void tearDown() {
        
    }
    
    private void listSongs() {
        List<Song> songs = repository.getSongs();
        for (Song song : songs) {
            LOG.info("{}", song);
        }
        
        LOG.info("song count: {}", songs.size());
    }

    private void listSongsOnCloud() throws Exception {
        for (B2BucketResponse bucket : b2ApiClient.listBuckets()) {
            LOG.info("Bucket: {}", bucket);
            for(B2FileInfoResponse file : b2ApiClient.listFileNames(bucket.getBucketId()).getFiles()) {
                LOG.info("-> File: {}", b2ApiClient.getFileInfo(file.getFileId()));
            }
        }
    }
    
    /**
     * Test of getDownloadURL method, of class B2CloudRepository.
     */
    @Test
    public void testGetDownloadURL() throws Exception {
        String prefix = "SampleCategory/";
        URL url = repository.getDownloadURL(fileId, prefix, 0);
        LOG.info("getDownloadURL({}): {}", fileId, url);
        assertNotNull("The download URL should not be null.", url);
        
        try {
            repository.getDownloadURL(fileId + "x", prefix, 0);
            fail("Invalid fileID should result in an exception.");
        } catch(CloudRepositoryException ex) {
            LOG.debug("Expected failure for bad fileId: {}", ex.getMessage());
        }
    }

    /**
     * Test of uploadSongFile method, of class B2CloudRepository.
     */
    @Test
    public void testUploadSongFile() throws Exception {
        Map<String, Object> searchParams = new HashMap<>();
        searchParams.put("artist", "Marvin Gaye");
        List<Song> songs = repository.getSongs(searchParams);
        assertTrue(songs.isEmpty());
        
        repository.uploadSongFile(musicDirectory, audioFile, "SampleCategory");
        //listSongs();
        //listSongsOnCloud();
        
        songs = repository.getSongs(searchParams);
        assertFalse(songs.isEmpty());
        
        for (Song song : songs) {
            assertNotNull("The song cloud file ID should not be null.", song.getCloudFileId());
        }
    }

    /**
     * Test of uploadSongDirectory method, of class B2CloudRepository.
     */
    @Test
    public void testUploadSongDirectory() throws Exception {
        Map<String, Object> searchParams = new HashMap<>();
        searchParams.put("artist", "Marvin Gaye");
        List<Song> songs = repository.getSongs(searchParams);
        assertTrue(songs.isEmpty());
        
        repository.uploadSongDirectory(musicDirectory, "SampleCategory");
        //listSongs();
        //listSongsOnCloud();
        
        songs = repository.getSongs(searchParams);
        assertFalse(songs.isEmpty());
        
        for (Song song : songs) {
            assertNotNull("The song cloud file ID should not be null.", song.getCloudFileId());
        }
    }
}
