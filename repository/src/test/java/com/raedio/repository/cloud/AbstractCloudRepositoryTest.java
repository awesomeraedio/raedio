package com.raedio.repository.cloud;

import static com.raedio.repository.cloud.AbstractCloudRepository.SUPPORTED_AUDIO_FORMATS;
import com.raedio.repository.model.Song;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import javax.persistence.PersistenceException;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Unit test for {@code AbstractCloudRepository}.
 *
 * @author Leonardo A. Mendoza III <leonardo.mendoza@gmail.com>
 */
public class AbstractCloudRepositoryTest {
    private static final Logger LOG = LoggerFactory.getLogger(AbstractCloudRepositoryTest.class);
    private static AbstractCloudRepository repository;
    private static File musicDirectory;
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        Properties properties = new Properties();
        properties.load(AbstractCloudRepositoryTest.class.getClassLoader().getResourceAsStream("B2CloudRepositoryTest.properties"));
        
        repository = Mockito.mock(AbstractCloudRepository.class, Mockito.CALLS_REAL_METHODS);
        Mockito.when(repository.getPersistenceUnitName()).thenReturn(AbstractCloudRepository.DEFAULT_PERSISTENCE_UNIT_NAME + "-h2");
        
        //repository = new AbstractCloudRepositoryImpl();
        musicDirectory = new File(properties.getProperty("MUSIC_DIRECTORY"));
        assertTrue(musicDirectory.exists());
        
        Map<File, String> uploadErrors = new TreeMap<>();
        
        LOG.debug("Uploading songs from directory {}", musicDirectory);
        for (File songFile : FileUtils.listFiles(musicDirectory, SUPPORTED_AUDIO_FORMATS.toArray(new String[]{}), true)) {
            try {
                String relativePath = repository.getRelativePath(musicDirectory, songFile);
                Song song = new Song(songFile, null, relativePath);
                repository.saveSongInfo(song);
            } catch (PersistenceException ex) {
                //LOG.error(String.format("Error uploading song: %s", songFile), ex);
                uploadErrors.put(songFile, ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage());
            }
        }
        
        for (Map.Entry<File, String> entry : uploadErrors.entrySet()) {
            LOG.error("*** Error uploading song: {} - cause: {}", entry.getKey(), entry.getValue());
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
        
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
        
    }

    /**
     * Test of getSongs method, of class AbstractCloudRepository.
     */
    @Test
    public void testGetSongs() {
        List<Song> songs = repository.getSongs();
        assertFalse("Song list should not be empty.", songs.isEmpty());
    }

    /**
     * Test of getSongs method, of class AbstractCloudRepository.
     */
    @Test
    public void testGetSongs_Map() {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("artist", "%marvin%");
        List<Song> songs = repository.getSongs(parameters);
        assertFalse("Valid search parameter should return a non-empty list of songs.", songs.isEmpty());
        
        parameters.put("artist", "%marvinx%");
        songs = repository.getSongs(parameters);
        assertTrue("Inalid search parameter should return an empty list of songs.", songs.isEmpty());
    }
    
    @Test
    public void testGetArtists() {
        List<String> artists = repository.getArtists();
        assertFalse("List of artists should not be empty.", artists.isEmpty());
    }
    
    @Test
    public void testGetAlbums() {
        List<String> albums = repository.getAlbums();
        assertFalse("List of albums should not be empty.", albums.isEmpty());
    }
    
    @Test
    public void testGetAlbumsByArtist() {
        List<String> albums = repository.getAlbums("%marvin%");
        assertFalse("List of albums of known artist should not be empty.", albums.isEmpty());
        albums = repository.getAlbums("%xmarvinx%");
        assertTrue("List of albums of unknown artist should be empty.", albums.isEmpty());
    }
}
