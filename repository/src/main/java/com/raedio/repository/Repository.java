package com.raedio.repository;

import com.raedio.repository.cloud.CloudRepositoryException;
import com.raedio.repository.model.Song;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * A simple repository for storage and retrieval of various content.
 *
 * @author Leonardo A. Mendoza III <leonardo.mendoza@gmail.com>
 */
public interface Repository {

    /**
     * Get the temporary download URL for a given the file ID.
     *
     * @param fileId the ID of the file to get the download URL for.
     * @param fileNamePrefix the file name prefix for which the URL is
     * accessible.
     * @param validDurationInSeconds the number of seconds the URL is valid.
     * @return the temporary URL for downloading the file.
     * @throws CloudRepositoryException if there is a problem with the API call.
     * @throws IOException if there was an error communicating with the API
     * service
     */
    URL getDownloadURL(String fileId, String fileNamePrefix, long validDurationInSeconds) throws CloudRepositoryException, IOException;

    /**
     * Uploads the specified song file to cloud storage in the specified cloud
     * storage prefix.
     *
     * @param baseDirectory the base directory parent of the file to upload.
     * Used to get the relative path of the file for the cloud storage.
     * @param songFile the song file to upload to cloud storage.
     * @param cloudFilePrefix the cloud storage file prefix.
     * @return the unique cloud storage file ID.
     * @throws CloudRepositoryException if there is a problem with the API call.
     * @throws IOException if there was an error communicating with the API
     * service
     */
    String uploadSongFile(File baseDirectory, File songFile, String cloudFilePrefix) throws CloudRepositoryException, IOException;

    /**
     * Recursively uploads the song files contained in the specified
     * {@code songDirectory} to cloud storage in the specified cloud storage
     * prefix.
     *
     * @param songDirectory the base directory parent of the song files to
     * upload.
     * @param cloudFilePrefix the cloud storage file prefix.
     * @throws CloudRepositoryException if there is a problem with the API call.
     * @throws IOException if there was an error communicating with the API
     */
    void uploadSongDirectory(File songDirectory, String cloudFilePrefix) throws CloudRepositoryException, IOException;

    /**
     * Gets all {@code Song}s.
     *
     * @return a list of all {@code Song}s stored in the repository.
     */
    List<Song> getSongs();

    /**
     * Gets all {@code Song}s that satisfy the specified parameters.
     *
     * @param parameters the JPQL parameters.
     * @return a list of all {@code Song}s stored in the repository that satisfy
     * the specified parameters.
     */
    List<Song> getSongs(Map<String, Object> parameters);

    /**
     * Gets all distinct artists in the repository.
     *
     * @return a list of all distinct artists in the repository.
     */
    List<String> getArtists();

    /**
     * Gets all distinct albums in the repository.
     *
     * @return a list of all distinct albums in the repository.
     */
    List<String> getAlbums();

    /**
     * Gets all distinct albums in the repository by the specified artist.
     *
     * @param artist the artist to search for albums.
     * @return a list of all distinct albums in the repository by the specified
     * artist.
     */
    List<String> getAlbums(String artist);
}
