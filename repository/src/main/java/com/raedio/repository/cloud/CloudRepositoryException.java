package com.raedio.repository.cloud;

/**
 * Wrapper exception for cloud storage API exceptions.
 *
 * @author Leonardo A. Mendoza III <leonardo.mendoza@gmail.com>
 */
public class CloudRepositoryException extends Exception {

    /**
     * Creates a new {@code CloudRepositoryException} instance.
     *
     * @param message the exception message.
     * @param cause the cause of the exception.
     */
    public CloudRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

}
