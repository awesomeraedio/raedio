package com.raedio.repository.cloud.b2.request;

import com.raedio.repository.cloud.b2.response.B2DownloadAuthorizationResponse;
import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import synapticloop.b2.exception.B2ApiException;
import synapticloop.b2.request.B2RequestProperties;
import synapticloop.b2.request.BaseB2Request;
import synapticloop.b2.response.B2AuthorizeAccountResponse;

/**
 *
 * @author lam
 */
public class B2DownloadAuthorizationRequest extends BaseB2Request {
    private static final Logger LOG = LoggerFactory.getLogger(B2DownloadAuthorizationRequest.class);
    private static final String B2_GET_DOWNLOAD_AUTHORIZATION = BASE_API_VERSION + "b2_get_download_authorization";

    /**
     * default to 5 minutes.
     */
    private static final long DEFAULT_DOWNLOAD_DURATION_IN_SECONDS = 300;

    public B2DownloadAuthorizationRequest(CloseableHttpClient client, B2AuthorizeAccountResponse b2AuthorizeAccountResponse, String bucketId, String fileNamePrefix, long validDurationInSeconds) {
        super(client, b2AuthorizeAccountResponse, b2AuthorizeAccountResponse.getApiUrl() + B2_GET_DOWNLOAD_AUTHORIZATION);
        Validate.notBlank(bucketId, "bucketId is a required parameter.");
        Validate.notNull(fileNamePrefix, "fileNamePrefix is a required parameter.");

        if (validDurationInSeconds <= 0) {
            validDurationInSeconds = DEFAULT_DOWNLOAD_DURATION_IN_SECONDS;
            LOG.debug("validDurationInSeconds set to default value of 5 minutes.");
        }

        addParameter(B2RequestProperties.KEY_BUCKET_ID, bucketId);
        
        if (StringUtils.isNotBlank(fileNamePrefix)) {
            addParameter("fileNamePrefix", fileNamePrefix);
        }
        
        addParameter("validDurationInSeconds", "" + validDurationInSeconds);
    }
    
    public B2DownloadAuthorizationResponse getResponse() throws B2ApiException, IOException {
        final CloseableHttpResponse httpResponse = executeGet();
        return new B2DownloadAuthorizationResponse(EntityUtils.toString(httpResponse.getEntity()));
    }
}
