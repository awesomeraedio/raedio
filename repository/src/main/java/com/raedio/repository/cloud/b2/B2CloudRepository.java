package com.raedio.repository.cloud.b2;

import com.raedio.repository.cloud.AbstractCloudRepository;
import com.raedio.repository.cloud.CloudRepositoryException;
import com.raedio.repository.cloud.b2.request.B2DownloadAuthorizationRequest;
import com.raedio.repository.cloud.b2.response.B2DownloadAuthorizationResponse;
import com.raedio.repository.model.Song;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.apache.http.HttpHeaders;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import synapticloop.b2.B2ApiClient;
import synapticloop.b2.exception.B2ApiException;
import synapticloop.b2.request.B2DownloadFileByNameRequest;
import synapticloop.b2.response.B2AuthorizeAccountResponse;
import synapticloop.b2.response.B2BucketResponse;
import synapticloop.b2.response.B2FileResponse;

/**
 * B2 {@code Repository} implementation.
 *
 * @author Leonardo A. Mendoza III <leonardo.mendoza@gmail.com>
 */
public class B2CloudRepository extends AbstractCloudRepository {

    /**
     * SLF4J Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(B2CloudRepository.class);

    /**
     * the B2 API account ID.
     */
    private static final String ACCOUNT_ID = "2889421eab56";

    /**
     * the B2 API application key.
     */
    private static final String APPLICATION_KEY = "00187ed360a4342975c9a611d215a656ee9ef967de";

    /**
     * the shared HTTP client.
     */
    private final CloseableHttpClient client;

    /**
     * the authorize account response.
     */
    private final B2AuthorizeAccountResponse b2AuthorizeAccountResponse;

    /**
     * cache of the bucket ID to bucket response mapping.
     */
    private Map<String, B2BucketResponse> cachedBucketMap = new HashMap<>();

    /**
     * the B2 API client wrapper.
     */
    private B2ApiClient b2ApiClient;

    /**
     * the default bucket ID to store the files to.
     */
    private String defaultBucketId;

    /**
     * Creates a new {@code B2CloudRepository} instance.
     *
     * @param defaultBucketId the default bucket ID to store the files to.
     * @throws CloudRepositoryException if there is a problem authenticating to
     * B2.
     * @throws IOException if there is a problem communicating with the B2
     * servers.
     */
    public B2CloudRepository(String defaultBucketId) throws CloudRepositoryException, IOException {
        Validate.notBlank(defaultBucketId, "The default bucket ID should not be null or blank.");
        this.defaultBucketId = defaultBucketId;

        try {
            client = HttpClients.createDefault();
            b2ApiClient = new B2ApiClient(client);
            b2AuthorizeAccountResponse = b2ApiClient.authenticate(ACCOUNT_ID, APPLICATION_KEY);
            LOG.debug("B2 API account authentication successful. Authorization response: {}", b2AuthorizeAccountResponse);
        } catch (B2ApiException ex) {
            throw new CloudRepositoryException("Error authenticating account.", ex);
        }
    }

    /**
     * Creates a new {@code B2CloudRepository} instance for unit testing.
     * @param client the HTTP client.
     * @param b2ApiClient the {@code B2ApiClient} preferably a mock object.
     * @param b2AuthorizeAccountResponse the B2 API authorization response.
     */
    protected B2CloudRepository(CloseableHttpClient client, B2ApiClient b2ApiClient, B2AuthorizeAccountResponse b2AuthorizeAccountResponse) {
        this.client = client;
        this.b2ApiClient = b2ApiClient;
        this.b2AuthorizeAccountResponse = b2AuthorizeAccountResponse;
    }
    
    /**
     * Helper method for getting the bucket name given the bucket ID.
     *
     * @param bucketId the ID of the bucket.
     * @return the name of the bucket.
     * @throws B2ApiException
     * @throws IOException
     */
    protected String getBucketName(String bucketId) throws B2ApiException, IOException {
        if (cachedBucketMap.containsKey(bucketId)) {
            return cachedBucketMap.get(bucketId).getBucketName();
        } else {
            for (B2BucketResponse bucket : b2ApiClient.listBuckets()) {
                if (bucket.getBucketId().equals(bucketId)) {
                    cachedBucketMap.put(bucketId, bucket);
                    return bucket.getBucketName();
                }
            }
        }

        return null;
    }

    /**
     * Helper method for creating a temporary download authorization token.
     *
     * @param bucketId the bucket ID.
     * @param fileNamePrefix the file name prefix.
     * @param validDurationInSeconds the amount of time in seconds that the
     * token is valid.
     * @return the temporary download authorization token.
     * @throws CloudRepositoryException if there is a problem getting the
     * download authorization token.
     * @throws IOException if there is a problem communicating with the B2
     * service.
     */
    private String getDownloadAuthorizationToken(String bucketId, String fileNamePrefix, long validDurationInSeconds) throws CloudRepositoryException, IOException {
        String result = null;
        B2DownloadAuthorizationRequest request = new B2DownloadAuthorizationRequest(client, b2AuthorizeAccountResponse, bucketId, fileNamePrefix, validDurationInSeconds);
        try {
            B2DownloadAuthorizationResponse response = request.getResponse();
            LOG.trace("Got download authorization response: {}", response);
            result = response.getAuthorizationToken();
        } catch (B2ApiException ex) {
            throw new CloudRepositoryException("Problem getting download authorization token.", ex);
        }

        return result;
    }

    @Override
    public URL getDownloadURL(String fileId, String fileNamePrefix, long validDurationInSeconds) throws CloudRepositoryException, IOException {

        try {
            B2FileResponse fileResponse = b2ApiClient.getFileInfo(fileId);
            String fileName = fileResponse.getFileName();
            String bucketId = fileResponse.getBucketId();
            String bucketName = getBucketName(bucketId);
            String authenticationToken = getDownloadAuthorizationToken(bucketId, fileNamePrefix, validDurationInSeconds);
            return new RaedioB2DownloadFileByNameRequest(client, b2AuthorizeAccountResponse, bucketName, fileName, authenticationToken).buildUri().toURL();
        } catch (B2ApiException ex) {
            throw new CloudRepositoryException(String.format("Problem getting download URL for fileId: %s", fileId), ex);
        }
    }

    @Override
    public String uploadSongFile(File baseDirectory, File songFile, String cloudFilePrefix) throws CloudRepositoryException, IOException {
        Validate.notNull(baseDirectory, "The base directory should not be null.");
        Validate.isTrue(baseDirectory.exists() && baseDirectory.isDirectory(), "The base directory should exist and is a directory.");
        Validate.notNull(songFile, "The file to upload should not be null.");
        Validate.isTrue(songFile.exists() && songFile.isFile(), "The file to upload should exist and not a directory.");
        Validate.isTrue(isFileSupported(songFile), String.format("The file %s is not a supported music file.", songFile));

        String cloudFileId = null;

        try {
            // upload the file to cloud storage and take note of the unique file ID.
            String relativePath = cloudFilePrefix + "/" + getRelativePath(baseDirectory, songFile);
            if (!Boolean.getBoolean("SKIP_CLOUD_UPLOAD")) {
                B2FileResponse fileResponse = b2ApiClient.uploadFile(defaultBucketId, relativePath, songFile);
                cloudFileId = fileResponse.getFileId();
            }

            // persist the song information to the database.
            Song song = new Song(songFile, cloudFileId, relativePath);
            saveSongInfo(song);
        } catch (IOException | B2ApiException | IllegalArgumentException ex) {
            throw new CloudRepositoryException(String.format("Problem uploading the file: %s", songFile), ex);
        }

        return cloudFileId;
    }

    @Override
    public void uploadSongDirectory(File songDirectory, String cloudFilePrefix) throws CloudRepositoryException, IOException {
        Validate.notNull(songDirectory, "The upload directory should not be null.");
        Validate.isTrue(songDirectory.exists() && songDirectory.isDirectory(), "The upload directory should exist and should be a directory.");

        LOG.debug("Uploading songs from directory {}", songDirectory);
        for (File songFile : FileUtils.listFiles(songDirectory, SUPPORTED_AUDIO_FORMATS.toArray(new String[]{}), true)) {
            uploadSongFile(songDirectory, songFile, cloudFilePrefix);
        }
    }

    /**
     * Extends {@code B2DownloadFileByNameRequest} in order to get the temporary
     * download URL.
     */
    private final class RaedioB2DownloadFileByNameRequest extends B2DownloadFileByNameRequest {

        public RaedioB2DownloadFileByNameRequest(CloseableHttpClient client, B2AuthorizeAccountResponse b2AuthorizeAccountResponse, String bucketName, String fileName, String authenticationToken) {
            super(client, b2AuthorizeAccountResponse, bucketName, fileName);
            addParameter(HttpHeaders.AUTHORIZATION, authenticationToken);
        }

        @Override
        public URI buildUri() throws IOException {
            return super.buildUri();
        }
    }
}
