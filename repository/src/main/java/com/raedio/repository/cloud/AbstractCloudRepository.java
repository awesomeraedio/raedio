package com.raedio.repository.cloud;

import com.raedio.repository.Repository;
import com.raedio.repository.model.Song;
import com.raedio.repository.model.SongPK;
import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.jaudiotagger.audio.SupportedFileFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parent base class for cloud storage servers.
 *
 * @author Leonardo A. Mendoza III <leonardo.mendoza@gmail.com>
 */
public abstract class AbstractCloudRepository implements Repository {

    /**
     * the SLF4J {@code Logger}.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AbstractCloudRepository.class);

    /**
     * the default JPA unit name.
     */
    public static final String DEFAULT_PERSISTENCE_UNIT_NAME = "radeo-repository-persistence-unit";

    /**
     * the file extensions of the supported audio formats.
     */
    public static final Set<String> SUPPORTED_AUDIO_FORMATS;

    static {
        SUPPORTED_AUDIO_FORMATS = new HashSet<>();
        for (SupportedFileFormat format : SupportedFileFormat.values()) {
            String suffix = format.getFilesuffix();
            if (!SupportedFileFormat.MP4.getFilesuffix().equals(suffix)) {
                SUPPORTED_AUDIO_FORMATS.add(suffix);
            }
        }
    }

    /**
     * the valid song key column names and types.
     */
    public static final Map<String, Class> SONG_KEY_COLUMNS;

    static {
        SONG_KEY_COLUMNS = new HashMap<>();
        for (Field field : SongPK.class.getDeclaredFields()) {
            for (Annotation annotation : field.getAnnotations()) {
                if (javax.persistence.Column.class.equals(annotation.annotationType())) {
                    SONG_KEY_COLUMNS.put(field.getName(), field.getType());
                }
            }
        }
    }

    /**
     * the valid song column names and types.
     */
    public static final Map<String, Class> SONG_COLUMNS;

    static {
        SONG_COLUMNS = new HashMap<>();
        for (Field field : Song.class.getDeclaredFields()) {
            if (!SongPK.class.equals(field.getType())) {
                for (Annotation annotation : field.getAnnotations()) {
                    if (javax.persistence.Column.class.equals(annotation.annotationType())) {
                        SONG_COLUMNS.put(field.getName(), field.getType());
                    }
                }
            }
        }
    }

    /**
     * the name of the JPA persistence unit to use.
     */
    private String persistenceUnitName;

    /**
     * the JPA {@code EntityManager}.
     */
    private EntityManager entityManager;

    /**
     * Creates a new {@code CloudRepository} instance.
     *
     * @param persistenceUnitName the name of the JPA persistence unit to use.
     */
    public AbstractCloudRepository(String persistenceUnitName) {
        this.persistenceUnitName = persistenceUnitName;
    }

    /**
     * Creates a new {@code CloudRepository} instance.
     */
    public AbstractCloudRepository() {

    }

    /**
     * Gets the name of the JPA persistence unit to use.
     *
     * @return the name of the JPA persistence unit to use.
     */
    public String getPersistenceUnitName() {
        if (persistenceUnitName == null) {
            persistenceUnitName = DEFAULT_PERSISTENCE_UNIT_NAME;
        }

        return persistenceUnitName;
    }

    /**
     * Gets the JPA {@code EntityManager}.
     *
     * @return the JPA {@code EntityManager}.
     */
    public EntityManager getEntityManager() {
        if (entityManager == null) {
            EntityManagerFactory factory = Persistence.createEntityManagerFactory(getPersistenceUnitName());
            entityManager = factory.createEntityManager();
        }

        return entityManager;
    }

    /**
     * Checks if the specified file is supported.
     *
     * @param songFile the file to check if supported or not.
     * @return {@code true} if the file is supported, {@code false} otherwise.
     */
    protected boolean isFileSupported(File songFile) {
        String name = songFile.getName();
        String extension = name.substring(name.lastIndexOf(".") + 1);
        return SUPPORTED_AUDIO_FORMATS.contains(extension);
    }

    /**
     * Helper method for getting the relative path as a string.
     *
     * @param baseDirectory the base directory parent of the file to upload.
     * Used to get the relative path of the file for the cloud storage.
     * @param songFile the song file to upload to cloud storage.
     * @return the relative path as a string.
     */
    protected String getRelativePath(File baseDirectory, File songFile) {
        Path path = Paths.get(baseDirectory.toURI()).relativize(Paths.get(songFile.toURI()));
        return path.toString().replaceAll("\\\\", "/");
    }
    
    /**
     * Extract the meta data from the music file and store it in the database.
     * @param song the {@code Song} data to persist.
     */
    protected void saveSongInfo(Song song) {
        try {
            getEntityManager().getTransaction().begin();
            getEntityManager().merge(song);
        } finally {
            if (getEntityManager().getTransaction().getRollbackOnly()) {
                getEntityManager().getTransaction().rollback();
                LOG.error("Rolled back saving song info to DB: {}", song);
            } else {
                getEntityManager().getTransaction().commit();
                LOG.trace("Saved song info to DB: {}", song);
            }
        }
    }

    /**
     * Checks if the query parameter name is valid.
     *
     * @param paramName the parameter name.
     * @return {@code true} if the parameter name is valid, {@code false}
     * otherwise.
     */
    protected boolean isValidQueryParameterName(String paramName) {
        return SONG_KEY_COLUMNS.keySet().contains(paramName) || SONG_COLUMNS.keySet().contains(paramName);
    }
    
    /**
     * Gets the type of the specified column.
     * @param columnName the column name.
     * @return the type of the specified column.
     */
    protected Class getColumnType(String columnName) {
        Class type = null;
        
        if (isValidQueryParameterName(columnName)) {
            if (SONG_KEY_COLUMNS.containsKey(columnName)) {
                type = SONG_KEY_COLUMNS.get(columnName);
            } else if (SONG_COLUMNS.containsKey(columnName)) {
                type = SONG_COLUMNS.get(columnName);
            }
        }
        
        return type;
    }

    @Override
    public List<Song> getSongs() {
        return getEntityManager().createQuery("SELECT s FROM Song s").getResultList();
    }

    @Override
    public List<Song> getSongs(Map<String, Object> parameters) {
        StringBuilder sql = new StringBuilder("SELECT s FROM Song s WHERE ");
        boolean first = true;
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            if (isValidQueryParameterName(entry.getKey()) && entry.getValue() != null) {
                if (!first) {
                    sql.append(" and ");
                }

                //TODO: if column type is a string and contains %, make it
                //uppercase and use like comparison.
                boolean useLike = String.class.equals(getColumnType(entry.getKey())) && ((String)entry.getValue()).contains("%");
                
                if (useLike) {
                    sql.append("UPPER(");
                }
                
                if (SONG_KEY_COLUMNS.keySet().contains(entry.getKey())) {
                    sql.append("s.songPK.");
                } else {
                    sql.append("s.");
                }

                sql.append(entry.getKey());
                
                if (useLike) {
                    sql.append(") like UPPER(:");
                } else {
                    sql.append(" = :");
                }
                
                sql.append(entry.getKey());
                
                if (useLike) {
                    sql.append(")");
                }

                first = false;
            }
        }

        LOG.debug("JPQL: {}", sql);

        Query query = getEntityManager().createQuery(sql.toString());
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            if (isValidQueryParameterName(entry.getKey()) && entry.getValue() != null) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }

        return query.getResultList();
    }

    @Override
    public List<String> getArtists() {
        return getEntityManager().createQuery("SELECT DISTINCT s.songPK.artist FROM Song s ORDER BY s.songPK.artist").getResultList();
    }

    @Override
    public List<String> getAlbums() {
        return getEntityManager().createQuery("SELECT DISTINCT s.songPK.album FROM Song s ORDER BY s.songPK.album").getResultList();
    }

    @Override
    public List<String> getAlbums(String artist) {
        Query query = getEntityManager().createQuery("SELECT DISTINCT s.songPK.album FROM Song s WHERE UPPER(s.songPK.artist) like UPPER(:artist) ORDER BY 1");
        query.setParameter("artist", artist);
        return query.getResultList();
    }

}
