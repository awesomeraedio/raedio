package com.raedio.repository.cloud.b2.response;

import com.raedio.repository.cloud.b2.request.B2DownloadAuthorizationRequest;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import synapticloop.b2.exception.B2ApiException;
import synapticloop.b2.request.B2RequestProperties;
import synapticloop.b2.response.BaseB2Response;

/**
 *
 * @author lam
 */
public class B2DownloadAuthorizationResponse extends BaseB2Response {
    private static final Logger LOG = LoggerFactory.getLogger(B2DownloadAuthorizationRequest.class);
    
    private final String bucketId;
    private final String fileNamePrefix;
    private final String authorizationToken;
    
    public B2DownloadAuthorizationResponse(String json) throws B2ApiException {
        super(json);
        LOG.trace("Parsing json: [{}]", json);
        bucketId = readString(B2RequestProperties.KEY_BUCKET_ID);
        fileNamePrefix = readString("fileNamePrefix");
        authorizationToken = readString("authorizationToken");
    }

    public String getBucketId() {
        return bucketId;
    }

    public String getFileNamePrefix() {
        return fileNamePrefix;
    }

    public String getAuthorizationToken() {
        return authorizationToken;
    }

    @Override
    protected Logger getLogger() {
        return LOG;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("bucketId", bucketId)
                .append("fileNamePrefix", fileNamePrefix)
                .append("authorizationToken", authorizationToken)
                .toString();
    }
}
