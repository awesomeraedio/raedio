package com.raedio.repository.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * The song's composite key class composed of the artist, album, and title.
 * @author Leonardo A. Mendoza III <leonardo.mendoza@gmail.com>
 */
@Embeddable
public class SongPK implements Serializable {

    @Column(name = "ARTIST", nullable = false)
    private String artist;
    @Column(name = "ALBUM", nullable = false)
    private String album;
    @Column(name = "ALBUM_YEAR")
    private int albumYear;
    @Column(name = "TITLE", nullable = false)
    private String title;

    public SongPK() {
        
    }

    public SongPK(String artist, String album, int albumYear, String title) {
        Validate.notBlank(artist, "The primary key artist should not be blank.");
        Validate.notBlank(album, "The primary key album should not be blank.");
        Validate.notBlank(title, "The primary key title should not be blank.");
        
        this.artist = artist;
        this.album = album;
        this.albumYear = albumYear;
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
    
    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public int getAlbumYear() {
        return albumYear;
    }

    public void setAlbumYear(int albumYear) {
        this.albumYear = albumYear;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.artist);
        hash = 41 * hash + Objects.hashCode(this.album);
        hash = 41 * hash + this.albumYear;
        hash = 41 * hash + Objects.hashCode(this.title);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SongPK other = (SongPK) obj;
        if (this.albumYear != other.albumYear) {
            return false;
        }
        if (!Objects.equals(this.artist, other.artist)) {
            return false;
        }
        if (!Objects.equals(this.album, other.album)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        return true;
    }

    
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("artist", artist)
                .append("album", album)
                .append("albumYear", albumYear)
                .append("title", title)
                .toString();
    }
}
