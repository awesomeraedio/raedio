package com.raedio.repository.model;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.PersistenceException;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Song tracks persistence object.
 *
 * @author Leonardo A. Mendoza III <leonardo.mendoza@gmail.com>
 */
@Entity
@Table(name = "SONGS", schema = "RAEDIO")
public class Song implements Serializable {

    /**
     * the SLF4J {@code Logger}.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Song.class);

    @EmbeddedId
    private SongPK songPK;
    @Column(name = "ALBUM_ARTIST")
    private String albumArtist;
    @Column(name = "BPM")
    private int bpm;
    @Column(name = "COMPOSER")
    private String composer;
    @Column(name = "COVER_ART")
    private String coverArt;
    @Column(name = "DISK_NO")
    private int diskNo;
    @Column(name = "DISK_TOTAL")
    private int diskTotal;
    @Column(name = "ENCODER")
    private String encoder;
    @Column(name = "GENRE")
    private String genre;
    @Column(name = "IS_COMPILATION")
    private boolean compilation;
    @Column(name = "LYRICS", columnDefinition = "CLOB")
    private String lyrics;
    @Column(name = "ORIGINAL_ARTIST")
    private String originalArtist;
    @Column(name = "RECORD_LABEL")
    private String recordLabel;
    @Column(name = "TRACK")
    private int track;
    @Column(name = "TRACK_TOTAL")
    private int trackTotal;
    @Column(name = "CLOUD_FILE_ID")
    private String cloudFileId;
    @Column(name = "CLOUD_FILE_NAME")
    private String cloudFileName;

    /**
     * Creates a new {@code Song} instance.
     *
     * @param songFile the song file.
     * @param cloudFileId the cloud storage file ID.
     * @param cloudFileName the cloud storage file name.
     * @throws PersistenceException if there is a problem extracting song data
     * from the specified song file.
     */
    public Song(File songFile, String cloudFileId, String cloudFileName) throws PersistenceException {
        this.cloudFileId = cloudFileId;
        this.cloudFileName = cloudFileName;

        try {
            AudioFile audioFile = AudioFileIO.read(songFile);
            Tag tag = audioFile.getTag();
            if (tag == null || tag.isEmpty()) {
                throw new PersistenceException(String.format("%s has no audio tags", songFile));
            }
            
            String artist = tag.getFirst(FieldKey.ARTIST);
            String album = tag.getFirst(FieldKey.ALBUM);
            String title = tag.getFirst(FieldKey.TITLE);
            int albumYear = parseAlbumYear(tag.getFirst(FieldKey.YEAR));
            songPK = new SongPK(artist, album, albumYear, title);

            for (FieldKey fieldKey : FieldKey.values()) {
                if (tag.hasField(fieldKey)) {
                    String value = tag.getFirst(fieldKey);
                    switch (fieldKey) {
                        case ALBUM_ARTIST:
                            albumArtist = value;
                            break;
                        case BPM:
                            bpm = Integer.parseInt(value);
                            break;
                        case COMPOSER:
                            composer = value;
                            break;
                        case COVER_ART:
                            //should point to URL
                            break;
                        case DISC_NO:
                            diskNo = Integer.parseInt(value);
                            break;
                        case DISC_TOTAL:
                            diskTotal = Integer.parseInt(value);
                            break;
                        case ENCODER:
                            encoder = value;
                            break;
                        case GENRE:
                            genre = value;
                            break;
                        case IS_COMPILATION:
                            compilation = Integer.parseInt(value) != 0;
                            break;
                        case LYRICS:
                            lyrics = value;
                            break;
                        case ORIGINAL_ARTIST:
                            originalArtist = value;
                            break;
                        case RECORD_LABEL:
                            recordLabel = value;
                            break;
                        case TRACK:
                            track = Integer.parseInt(value);
                            break;
                        case TRACK_TOTAL:
                            trackTotal = Integer.parseInt(value);
                            break;
                        default:
                    }
                }
            }
        } catch (IOException | CannotReadException | InvalidAudioFrameException | ReadOnlyFileException | TagException | IllegalArgumentException ex) {
            throw new PersistenceException(String.format("Problem extracting data from %s", songFile), ex);
        }
    }

    private int parseAlbumYear(String value) throws NumberFormatException {
        int albumYear = 0;
        if (value.length() > 4) {
            Pattern yearPattern = Pattern.compile("\\d\\d\\d\\d");
            Matcher matcher = yearPattern.matcher(value);
            if (matcher.find()) {
                value = value.substring(matcher.start(), matcher.end());
                albumYear = Integer.parseInt(value);
            }
        } else {
            try {
                albumYear = Integer.parseInt(value);
            } catch (NumberFormatException ex) {
                LOG.trace("Year not defined for {}", songPK);
            }
        }

        return albumYear;
    }

    public Song() {

    }

    public SongPK getSongPK() {
        return songPK;
    }

    public void setSongPK(SongPK songPK) {
        this.songPK = songPK;
    }

    public String getAlbumArtist() {
        return albumArtist;
    }

    public void setAlbumArtist(String albumArtist) {
        this.albumArtist = albumArtist;
    }

    public int getBpm() {
        return bpm;
    }

    public void setBpm(int bpm) {
        this.bpm = bpm;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public String getCoverArt() {
        return coverArt;
    }

    public void setCoverArt(String coverArt) {
        this.coverArt = coverArt;
    }

    public int getDiskNo() {
        return diskNo;
    }

    public void setDiskNo(int diskNo) {
        this.diskNo = diskNo;
    }

    public int getDiskTotal() {
        return diskTotal;
    }

    public void setDiskTotal(int diskTotal) {
        this.diskTotal = diskTotal;
    }

    public String getEncoder() {
        return encoder;
    }

    public void setEncoder(String encoder) {
        this.encoder = encoder;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public boolean isCompilation() {
        return compilation;
    }

    public void setCompilation(boolean compilation) {
        this.compilation = compilation;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public String getOriginalArtist() {
        return originalArtist;
    }

    public void setOriginalArtist(String originalArtist) {
        this.originalArtist = originalArtist;
    }

    public String getRecordLabel() {
        return recordLabel;
    }

    public void setRecordLabel(String recordLabel) {
        this.recordLabel = recordLabel;
    }

    public int getTrack() {
        return track;
    }

    public void setTrack(int track) {
        this.track = track;
    }

    public int getTrackTotal() {
        return trackTotal;
    }

    public void setTrackTotal(int trackTotal) {
        this.trackTotal = trackTotal;
    }

    /**
     * Gets the song's cloud storage file ID.
     *
     * @return the song's cloud storage file ID.
     */
    public String getCloudFileId() {
        return cloudFileId;
    }

    /**
     * Sets the song's cloud storage file ID.
     *
     * @param cloudFileId the song's cloud storage file ID.
     */
    public void setCloudFileId(String cloudFileId) {
        this.cloudFileId = cloudFileId;
    }

    /**
     * Gets the song's cloud storage file name.
     *
     * @return the song's cloud storage file name.
     */
    public String getCloudFileName() {
        return cloudFileName;
    }

    /**
     * Sets the song's cloud storage file name.
     *
     * @param cloudFileName the song's cloud storage file name.
     */
    public void setCloudFileName(String cloudFileName) {
        this.cloudFileName = cloudFileName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("songPK", songPK)
                .append("albumArtist", albumArtist)
                .append("bpm", bpm)
                .append("cloudFileId", cloudFileId)
                .append("cloudFileName", cloudFileName)
                .append("compilation", compilation)
                .append("composer", composer)
                .append("coverArt", coverArt)
                .append("diskNo", diskNo)
                .append("diskTotal", diskTotal)
                .append("encoder", encoder)
                .append("genre", genre)
                .append("lyrics", lyrics)
                .append("originalArtist", originalArtist)
                .append("recordLabel", recordLabel)
                .append("track", track)
                .append("trackTotal", trackTotal)
                .toString();
    }
}
