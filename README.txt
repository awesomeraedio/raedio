Last updated: 2017-01-25
========================

Overview
========

A music streaming app that has social features (follow other users, post status,
like and comment on posts, etc) and allows users to create radio format
play lists.
